/**
 * index
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { IDownloaderConstructor } from '../interface/downloader';
import github from './github';
import gitlab from './gitlab';

const downloaders : { [strategy: string] : IDownloaderConstructor } = ({
  github,
  gitlab,
});

export default downloaders;

/**
 * gitlab
 */

/* Node modules */

/* Third-party modules */
import request from 'request-promise-native';

/* Files */
import Downloader from '../lib/downloader';

export default class GitLab extends Downloader {

  protected async getUserId() : Promise<number> {
    const url = `https://gitlab.com/api/v4/users?username=${this.target}`;
    const opts = {
      json: true,
    };

    const [body] = await request.get(url, opts);

    if (body && body.id) {
      return body.id;
    }

    throw new Error(`Unknown user ${this.target}`);
  }

  async getKeys() : Promise<string[]> {
    const id = await this.getUserId();

    const url = `https://gitlab.com/api/v4/users/${id}/keys`;
    const opts = {
      json: true,
    };

    const body = await request.get(url, opts);

    return body.map(({ key } : { key: string }) => key);
  }

}

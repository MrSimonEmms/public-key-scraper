/**
 * github
 */

/* Node modules */

/* Third-party modules */
import request from 'request-promise-native';

/* Files */
import Downloader from '../lib/downloader';

export default class GitHub extends Downloader {

  async getKeys() : Promise<string[]> {
    const url = `https://api.github.com/users/${this.target}/keys`;
    const opts = {
      headers: {
        'user-agent': this.userAgent,
      },
      json: true,
    };

    const body = await request.get(url, opts);

    return body.map(({ key } : { key: string }) => key);
  }

}

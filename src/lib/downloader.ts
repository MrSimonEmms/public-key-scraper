/**
 * downloader
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { IDownloader } from '../interface/downloader';

export default abstract class Downloader implements IDownloader {

  /* User agent is sometimes expected */
  protected userAgent = 'public-key-scraper';

  constructor(public target: string) {}

  abstract getKeys() : Promise<string[]>;

}

/**
 * scraper
 */

/* Node modules */
import path from 'path';

/* Third-party modules */
import fs from 'fs-extra';

/* Files */
import { IScraper } from '../interface/scraper';
import downloaders from '../downloaders';
import { IDownloader } from '../interface/downloader';

export default class Scraper {

  get directory() : string {
    return path.dirname(this.target);
  }

  get source() : IDownloader[] {
    return this.opts.source
      .map(({ type, target }) => new downloaders[type](target));
  }

  get target() : string {
    return path.resolve(this.opts.target);
  }

  protected constructor(protected opts: IScraper) {
    console.log('New scraper %s', JSON.stringify(opts));
  }

  async generateFile(keys: Set<string>) {
    /* Create the new file */
    const fileItems : string[] = [
      `# Generated ${new Date().toISOString()}`,
    ];

    const iterator = keys.entries();
    for (const [entry] of iterator) {
      fileItems.push(entry);
    }
    /* Ensure file new line */
    fileItems.push('');

    const file = fileItems.join('\n');

    /* Save the file */
    console.log('Creating directory %s', this.directory);
    await fs.mkdirp(this.directory);

    console.log('Creating file %s', this.target);
    await fs.writeFile(this.target, file, {
      encoding: 'utf-8',
    });
  }

  async getKeys() : Promise<Set<string>> {
    const tasks = this.source
      .map((item) => {
        console.log(`Downloading keys for ${item.target}`);

        return item.getKeys();
      });

    /* Get all the keys */
    const result = await Promise.all(tasks);

    /* Flatten and remove duplicates */
    const keys = new Set<string>();

    result.forEach((keyGroup) => {
      keyGroup.forEach((key) => {
        keys.add(key);
      });
    });

    return keys;
  }

  public static async update(opts: IScraper) : Promise<void> {
    const scraper = new Scraper(opts);

    /* Download the keys */
    const keys = await scraper.getKeys();

    await scraper.generateFile(keys);

    console.log(`Saved ${keys.size} keys`);
  }

}

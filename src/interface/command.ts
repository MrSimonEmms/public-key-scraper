/**
 * command
 */

/* Node modules */

/* Third-party modules */
import { Arguments } from 'yargs';

/* Files */

export interface IDownload {
  target: string;
  github?: string[];
}

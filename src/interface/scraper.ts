/**
 * scraper
 */

/* Node modules */

/* Third-party modules */

/* Files */

export interface ISource {
  type: string;
  target: string;
}

export interface IScraper {
  source: ISource[];
  target: string;
}

/**
 * downloader
 */

/* Node modules */

/* Third-party modules */

/* Files */

export interface IDownloaderConstructor {
  new(target: string) : IDownloader;
}

export interface IDownloader {
  target: string;
  getKeys() : Promise<string[]>;
}

#!/usr/bin/env node

/**
 * public-key-scraper
 */

/* Node modules */
import os from 'os';

/* Third-party modules */
import yargs, { Arguments } from 'yargs';

/* Files */
import { IDownload } from '../interface/command';
import { ISource, IScraper } from '../interface/scraper';
import Scraper from '../lib/scraper';

export default yargs
  .scriptName('public-key-scraper')
  .usage('$0 <cmd> [args]')
  .command({
    command: 'download',
    describe: 'Download keys',
    builder: (yargs: any) => yargs.options({
      github: {
        describe: 'GitHub user',
        type: 'array',
      },
      gitlab: {
        describe: 'GitLab user',
        type: 'array',
      },
      target: {
        describe: 'File to store your keys',
        default: `${os.homedir()}/.ssh/authorized_keys`,
        type: 'string',
      },
    }),
    async handler(args: Arguments<IDownload>) {
      const sourceTypes = [
        'gitlab',
        'github',
      ];

      const source : ISource[] = sourceTypes.reduce(
        (result: ISource[], type) => {
          const source = (<any> args)[type];

          if (source) {
            source.forEach((target: string) => {
              result.push({
                type,
                target,
              });
            });
          }

          return result;
        },
        []);

      const opts : IScraper = {
        source,
        target: args.target,
      };

      if (opts.source.length === 0) {
        throw new Error('Please select at least one source');
      }

      await Scraper.update(opts);
    },
  })
  .alias('v', 'version')
  .alias('h', 'help')
  .demandCommand()
  .recommendCommands()
  .help()
  .parse();

FROM node:10-alpine as builder
ARG APP_NAME
ARG BUILD_ID
ARG VERSION
WORKDIR /opt/app
ADD . .
RUN npm i \
  && npm run compile
ENV APP_NAME="${APP_NAME}"
ENV BUILD_ID="${BUILD_ID}"
ENV VERSION="${VERSION}"
USER node

FROM node:10-alpine
ARG APP_NAME
ARG BUILD_ID
ARG VERSION
WORKDIR /opt/app
COPY --from=builder /opt/app/package.json .
COPY --from=builder /opt/app/package-lock.json .
COPY --from=builder /opt/app/dist/ ./dist
COPY --from=builder /opt/app/node_modules/ ./node_modules
RUN npm prune --production
ENV APP_NAME="${APP_NAME}"
ENV BUILD_ID="${BUILD_ID}"
ENV VERSION="${VERSION}"
ENV NODE_ENV="production"
ENTRYPOINT [ "node", "-r", "source-map-support/register", "./dist/bin/public-key-scraper" ]


# Public Key Scraper

Extracts public SSH keys from known sources into authorized_keys format

```bash
public-key-scraper <cmd> [args]

Commands:
  public-key-scraper download  Download keys

Options:
  -v, --version  Show version number                                   [boolean]
  -h, --help     Show help                                             [boolean]
```

## Download

```bash
public-key-scraper download

Download keys

Options:
  --github       GitHub user                                             [array]
  --gitlab       GitLab user                                             [array]
  --target       File to store your keys
                          [string] [default: "~/.ssh/authorized_keys"]
  -v, --version  Show version number                                   [boolean]
  -h, --help     Show help                                             [boolean]
```
